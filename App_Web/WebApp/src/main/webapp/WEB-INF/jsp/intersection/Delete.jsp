<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>

<html>
	<t:GenericPage>
		<jsp:body>
			<div class="page-header">
			  <h3>Delete Intersection<small style="color:red">&nbsp;&nbsp;${Error}</small></h3>
			</div>
			<form method="post">
				<input name="entityId" type="hidden" value="${Intersection.id}" >
				<div class="form-horizontal">
					<div class="form-group">
						<label class="control-label col-md-2">Id</label>
						<div class="col-md-10">
							<input name="intersectionAssoId" type="text" class="form-control" readonly="readonly" value="${Intersection.id}">
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-md-2">Name</label>
						<div class="col-md-10">
							<input name="intersectionName" type="text" class="form-control" readonly="readonly" value="${Intersection.name}">
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-md-2">Location</label>
						<div class="col-md-10">
							<input name="intersectionLocation" type="text" class="form-control" readonly="readonly" value="${Intersection.location}">
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-md-2">Exist1</label>
						<div class="col-md-2">
							<input name="intersectionExist1" type="checkbox" class="form-control" readonly="readonly" checked="${Intersection.exist1}">
						</div>
						<label class="control-label col-md-2">Camid1</label>
						<div class="col-md-6">
							<input name="intersectionCamid1" type="number" class="form-control" readonly="readonly" value="${Intersection.camid1}">
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-md-2">Exist2</label>
						<div class="col-md-2">
							<input name="intersectionExist2" type="checkbox" class="form-control" readonly="readonly" checked="${Intersection.exist2}">
						</div>
						<label class="control-label col-md-2">Camid2</label>
						<div class="col-md-6">
							<input name="intersectionCamid2" type="number" class="form-control" readonly="readonly" value="${Intersection.camid2}">
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-md-2">Exist3</label>
						<div class="col-md-2">
							<input name="intersectionExist3" type="checkbox" class="form-control" readonly="readonly" checked="${Intersection.exist3}">
						</div>
						<label class="control-label col-md-2">Camid3</label>
						<div class="col-md-6">
							<input name="intersectionCamid3" type="number" class="form-control" readonly="readonly" value="${Intersection.camid3}">
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-md-2">Exist4</label>
						<div class="col-md-2">
							<input name="intersectionExist4" type="checkbox" class="form-control" readonly="readonly" checked="${Intersection.exist4}">
						</div>
						<label class="control-label col-md-2">Camid4</label>
						<div class="col-md-6">
							<input name="intersectionCamid4" type="number" class="form-control" readonly="readonly" value="${Intersection.camid4}">
						</div>
					</div>
					
					<div class="form-group">
						<input type="submit" name="formDelete" value="Supprimer" class="btn btn-danger">
						<input type="button" value="Retour" onclick="location.href='./Index'" class="btn btn-primary">
					</div>
				</div>
			</form>
		</jsp:body>
	</t:GenericPage>
</html>