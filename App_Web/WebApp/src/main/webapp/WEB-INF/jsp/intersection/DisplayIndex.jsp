<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>

<html>
	<t:GenericPage>
		<jsp:body>
			<div id="index-header">
				<h3 class="col-md-6">Index des intersections<small style="color:red">&nbsp;&nbsp;${Error}</small></h3>
			</div>
			<table class="table">
				<thead>
					<tr>
						<th>ID</th>
						<th>Name</th>
						<th>Location</th>
						<th>Actions</th>
					</tr>
				</thead>
				<tbody>
					<c:forEach var="Intersection" items="${Intersections}">
						<tr>
							<td>${Intersection.id}</td>
							<td>${Intersection.name}</td>
							<td>${Intersection.location}</td>
							<td>
								<a href="Display?Id=${Intersection.id}"><span class="glyphicon glyphicon-list"></span></a>
							</td>
						</tr>
					</c:forEach>
				</tbody>
			</table>
		</jsp:body>
	</t:GenericPage>
</html>
