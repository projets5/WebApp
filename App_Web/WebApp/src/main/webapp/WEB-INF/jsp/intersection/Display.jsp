<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>

<html>
	<t:GenericPage>
		<jsp:body>	
			<script src="https://cdnjs.cloudflare.com/ajax/libs/socket.io/2.1.0/socket.io.js"></script>			
			<div class="row">
			  <div class="col-lg-6">
			    <img src="http://localhost:8080/upload/image/${Intersection.camid1}">
			  </div>
			  <div class="col-lg-6">
			    <img src="http://localhost:8080/upload/image/${Intersection.camid2}">
			  </div>
			</div>
			<div class="row">
			  <div class="col-lg-6">
			    <img src="http://localhost:8080/upload/image/${Intersection.camid3}">
			  </div>
			  <div class="col-lg-6">
			    <img src="http://localhost:8080/upload/image/${Intersection.camid4}">
			  </div>
			</div>
			<div class="row">
				<div class=col-lg-3>
					<button id="btn_cam_0" onClick="btnCamOnClick(0, ${Intersection.camid1})" class="btn btn-default">1</button>
				</div>
				<div class=col-lg-3>
					<button id="btn_cam_1" onClick="btnCamOnClick(1, ${Intersection.camid2})" class="btn btn-default">2</button>
				</div>
				<div class=col-lg-3>
					<button id="btn_cam_2" onClick="btnCamOnClick(2, ${Intersection.camid3})" class="btn btn-default">3</button>
				</div>
				<div class=col-lg-3>
					<button id="btn_cam_3" onClick="btnCamOnClick(3, ${Intersection.camid4})" class="btn btn-default">4</button>
				</div>
			</div>
			<div class="row">
				<div id="statusMessage" class=col-lg-12>
				
				</div>
			</div>
			<script>
				var sio = io("http://localhost:8080");
				var intersectionId = ${Intersection.id};
				/* var CamId1 = ${Intersection.camid1};
				var CamId2 = ${Intersection.camid2};
				var CamId3 = ${Intersection.camid3};
				var CamId4 = ${Intersection.camid4}; */
				
				var state = {};
				
				sio.on("connect", () => {
					console.log("CONNECTED")
					
					sio.emit("GET_INITIAL_STATE", intersectionId,  (initialState) => {
						
						state = initialState;
						
						this.updateUi();
					})
					
				})
				
				sio.on("UPDATE_CAM_STATE", (camId, camState) => {
					state[camId] = camState;
					
					
					
					this.updateUi();
				})
				
				this.btnCamOnClick = (camIndex, camId) =>  {
					state[camId] = -1; // Manual overwrite
					
					this.updateUi();
					
				}
				
				this.updateUi = () => {
					$("#statusMessage").html("")
					
					Object.keys(state).forEach((camId, camIndex) => {
						var camState = state[camId];
						var $btnCam = $("#btn_cam_" + camIndex);
						
						$btnCam.removeClass();
						$btnCam.addClass("btn");
						
						switch("" + camState) {
							case "0": { // RED
								$btnCam.addClass("btn-danger")
								break;
							}
							case "1": { // GREEN
								$btnCam.addClass("btn-success")
								
								break;
							}
							case "2": { // YELLOW
								$btnCam.addClass("btn-warning")
								
								break;
							}
							default: {
								$btnCam.addClass("btn-default")
								
								$("#statusMessage").html("Processing...")
								
								sio.emit("MANUAL_UPDATE", camId);
								
								break;
							}
						}
						
					})
					
				}
				//sio.emit("MANUAL_OVERWRITE", , true)
			</script>
		</jsp:body>
	</t:GenericPage>
</html>


