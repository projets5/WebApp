<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>

<html>
	<t:GenericPage>
		<jsp:body>
			<div class="page-header">
			  <h3>Create Intersection<small style="color:red">&nbsp;&nbsp;${Error}</small></h3>
			</div>
			<form method="post">
				<div class="form-horizontal">
					<div class="form-group">
						<label class="control-label col-md-2">Name</label>
						<div class="col-md-10">
							<input name="intersectionName" type="text" class="form-control">
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-md-2">Location</label>
						<div class="col-md-10">
							<input name="intersectionLocation" type="text" class="form-control">
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-md-2">Exist1</label>
						<div class="col-md-2">
							<input name="intersectionExist1" type="checkbox" class="form-control">
						</div>
						<label class="control-label col-md-2">Camid1</label>
						<div class="col-md-6">
							<input name="intersectionCamid1" type="number" class="form-control">
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-md-2">Exist2</label>
						<div class="col-md-2">
							<input name="intersectionExist2" type="checkbox" class="form-control">
						</div>
						<label class="control-label col-md-2">Camid2</label>
						<div class="col-md-6">
							<input name="intersectionCamid2" type="number" class="form-control">
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-md-2">Exist3</label>
						<div class="col-md-2">
							<input name="intersectionExist3" type="checkbox" class="form-control">
						</div>
						<label class="control-label col-md-2">Camid3</label>
						<div class="col-md-6">
							<input name="intersectionCamid3" type="number" class="form-control">
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-md-2">Exist4</label>
						<div class="col-md-2">
							<input name="intersectionExist4" type="checkbox" class="form-control">
						</div>
						<label class="control-label col-md-2">Camid4</label>
						<div class="col-md-6">
							<input name="intersectionCamid4" type="number" class="form-control">
						</div>
					</div>
										
					<div class="form-group">
						<input type="submit" value="Enregistrer" class="btn btn-success">
						<input type="button" value="Retour" onclick="location.href='./Index'" class="btn btn-primary">
					</div>
				</div>
			</form>
		</jsp:body>
	</t:GenericPage>
</html>