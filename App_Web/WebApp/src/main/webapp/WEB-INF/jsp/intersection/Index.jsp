<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>

<html>
	<t:GenericPage>
		<jsp:body>
			<div id="index-header">
				<h3 class="col-md-6">Index des intersections<small style="color:red">&nbsp;&nbsp;${Error}</small></h3>
				<input type="button" value="Ajouter un formulaire" onclick="location.href='./Create'" class="btn btn-info col-md-4">
			</div>
			<table class="table">
				<thead>
					<tr>
						<th>ID</th>
						<th>Name</th>
						<th>Location</th>
						<th>Actions</th>
					</tr>
				</thead>
				<tbody>
					<c:forEach var="Intersection" items="${Intersections}">
						<tr>
							<td>${Intersection.id}</td>
							<td>${Intersection.name}</td>
							<td>${Intersection.location}</td>
							<td>
								<a href="Detail?Id=${Intersection.id}"><span class="glyphicon glyphicon-list"></span></a>
								<a href="Edit?Id=${Intersection.id}"><span class="glyphicon glyphicon-edit" style="color:orange"></span></a>
								<a href="Delete?Id=${Intersection.id}"><span class="glyphicon glyphicon-remove" style="color:red"></span></a>
							</td>
						</tr>
					</c:forEach>
				</tbody>
			</table>
		</jsp:body>
	</t:GenericPage>
</html>
