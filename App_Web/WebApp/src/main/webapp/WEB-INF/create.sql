CREATE SCHEMA IF NOT EXISTS watatow_prod;
CREATE TABLE IF NOT EXISTS watatow_prod.intersections (
	id int NOT NULL PRIMARY KEY AUTO_INCREMENT,
    name varchar(255) NOT NULL,
    location varchar(255),
    camid1 int,
    camid2 int,
    camid3 int,
    camid4 int,
    exist1 BOOLEAN NOT NULL DEFAULT 0,
    exist2 BOOLEAN NOT NULL DEFAULT 0,
    exist3 BOOLEAN NOT NULL DEFAULT 0,
    exist4 BOOLEAN NOT NULL DEFAULT 0
);