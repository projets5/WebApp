<%@ tag description="Generic Page Template" language="java" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<head>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
	<script src="${pageContext.request.contextPath}/js/StickyHeader.js"></script>
	
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
	<link rel="stylesheet" href="${pageContext.request.contextPath}/css/StickyFooter.css">
	<link rel="stylesheet" href="${pageContext.request.contextPath}/css/GenericPage.css">	

	<title>Watatow Inc.</title>
</head>

<header>
    <div class="container-fluid" id="testa">
    	<div class="testa">
			<p> WATATOW Inc.</p>
		</div>
	</div>
</header>

<nav class="navbar navbar-default">
	<div class="container-fluid" id="lanav">
	    <div class="navbar-header">
	    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
	        <span class="sr-only">Toggle navigation</span>
	        <span class="icon-bar"></span>
	        <span class="icon-bar"></span>
	        <span class="icon-bar"></span>
        </button>
	      	<a class="navbar-header" href="/PiWeb/Home">Accueil</a>		   
	    </div>

	    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1"> 
	    	<ul class="nav navbar-nav">		
	        	<li class="dropdown">
			        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Administration <span class="caret"></span></a>
			        <ul class="dropdown-menu">
		    			<li><a href="/WebApp/Intersection/Index">Intersections</a></li>	
			        </ul>
		        </li>		
	        	<li><a href="/PiWeb/User/Index">Usagers</a></li>
		        <li><a href="/PiWeb/Login">Se déconnecter</a></li>
	      	</ul>
	     </div>
	</div>
</nav>

<div class="container">
	<jsp:doBody/>
</div>

<footer class="footer">
     <p class="text-muted">© 2018 Watatow Inc. Tous droits réservés.</p>
</footer>

