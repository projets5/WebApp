
document.addEventListener("scroll", function() {
	var navigation = document.getElementsByClassName("navbar")[0];
	var container = document.getElementsByClassName("container")[0];
	
	if (window.scrollY > 255) {
		navigation.className = "navbar navbar-default navbar-sticky";
		container.className = "container container-sticky";
	}
	else {
		navigation.className = "navbar navbar-default";
		container.className = "container";
	}
});
