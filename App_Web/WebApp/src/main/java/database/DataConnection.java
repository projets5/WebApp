package database;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DataConnection
{
	private String hostName;
	private String userName;
	private String password;

	private Connection connection;

	public DataConnection(String _hostName, String _userName, String _password) throws SQLException
	{
		this.hostName = _hostName;
		this.userName = _userName;
		this.password = _password;

		this.connection = DriverManager.getConnection(this.hostName, this.userName, this.password);
	}
	
	public static String getInitialiserString(InputStream in) {
		String s;
		StringBuffer sb = new StringBuffer();
		 
	    try {
	        BufferedReader br = new BufferedReader(new InputStreamReader(in));
	        while((s = br.readLine()) != null)
	        {
	        	sb.append(s);
	        }
	        br.close();
	        
	        s = new String();
	        String[] inst = sb.toString().split(";");
	        for(int i = 0; i < inst.length; i++) {
	        	s += inst[i] += "\\;";
	        }
	        
	        return s;
	    } catch(Exception e) {
	    	e.printStackTrace();
	    	return "";
	    }
    }

	public Connection getConnection()
	{
		return this.connection;
	}

}
