package database;

import java.sql.ResultSet;
import java.sql.SQLException;

public abstract class DataEntity {
	public int Id;
	
	public abstract void loadEntity(ResultSet resultSet) throws SQLException;
	public abstract void reloadEntity() throws SQLException;
	public abstract void createEntity() throws SQLException;
	public abstract void updateEntity() throws SQLException;
	public abstract void deleteEntity() throws SQLException;

	public abstract int getId();
	public abstract void setId(int id);
}
