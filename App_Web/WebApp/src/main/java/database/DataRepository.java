package database;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import entity.Intersection;

public class DataRepository {
	public static final String IntersectionSelection = "select * from Watatow_Prod.Intersections";
	
	public static final String IntersectionCreate = "insert into Watatow_Prod.Intersections";
	
	public static final String IntersectionUpdate = "update Watatow_Prod.Intersections";
	
	public static final String IntersectionDelete = "delete from Watatow_Prod.Intersections";
	
	public static DataSet<Intersection> IntersectionDataSet;
	
	public static DataConnection Connection;
	public static Statement Statement;
	
	public static void LoadDataRepository(DataConnection connection) throws SQLException, IllegalAccessException, InstantiationException
	{
		Connection = connection;
		Statement = Connection.getConnection().createStatement();

		ResultSet resultSet;

		resultSet = Statement.executeQuery(IntersectionSelection);
		IntersectionDataSet = DataRepository.<Intersection>LoadDataSet(Intersection.class, resultSet);
	}

	
	public static <T extends DataEntity> DataSet<T> LoadDataSet(Class<T> type, ResultSet resultSet) throws SQLException, IllegalAccessException, InstantiationException
	{
		DataSet<T> dataSet = new DataSet<T>();
		while(resultSet.next())
		{
			T model = type.newInstance();
			model.loadEntity(resultSet);
			dataSet.add(model);
		}
		return dataSet;
	}
}
