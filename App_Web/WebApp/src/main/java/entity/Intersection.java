package entity;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import database.DataEntity;
import database.DataRepository;

public class Intersection extends DataEntity {
	
	public String Name;
	public String Location;
	public int Camid1;
	public int Camid2;
	public int Camid3;
	public int Camid4;
	public boolean Exist1;
	public boolean Exist2;
	public boolean Exist3;
	public boolean Exist4;
	
	public Intersection() {
		
	}
	
	public Intersection(String Name, String Location,
			int Camid1, int Camid2, int Camid3, int Camid4,
			boolean Exist1, boolean Exist2, boolean Exist3, boolean Exist4) {
		this.Name = Name; this.Location = Location;
		this.Camid1 = Camid1; this.Camid2 = Camid2; this.Camid3 = Camid3; this.Camid4 = Camid4;
		this.Exist1 = Exist1; this.Exist2 = Exist2; this.Exist3 = Exist3; this.Exist4 = Exist4;
	}

	@Override
	public void loadEntity(ResultSet resultSet) throws SQLException {
		Id = resultSet.getInt("Id");
		Name = resultSet.getString("Name");
		Location = resultSet.getString("Location");
		
		Camid1 = resultSet.getInt("Camid1");
		Camid2 = resultSet.getInt("Camid2");
		Camid3 = resultSet.getInt("Camid3");
		Camid4 = resultSet.getInt("Camid4");
		
		Exist1 = resultSet.getBoolean("Exist1");
		Exist2 = resultSet.getBoolean("Exist2");
		Exist3 = resultSet.getBoolean("Exist3");
		Exist4 = resultSet.getBoolean("Exist4");
	}

	@Override
	public void reloadEntity() throws SQLException {
		String querry = DataRepository.IntersectionSelection + " WHERE Id = ?";

		PreparedStatement preparedStatement = DataRepository.Connection.getConnection().prepareStatement(querry, Statement.RETURN_GENERATED_KEYS);
		preparedStatement.setInt(1, this.Id);

		ResultSet resultSet = preparedStatement.executeQuery();
		if(resultSet.next())
			loadEntity(resultSet);
		preparedStatement.close();
	}

	@Override
	public void createEntity() throws SQLException {
		String querry = DataRepository.IntersectionCreate + " (Name, Location, Camid1, Camid2, Camid3, Camid4, Exist1, Exist2, Exist3, Exist4) VALUES (?,?,?,?,?,?,?,?,?,?)";

		PreparedStatement preparedStatement = DataRepository.Connection.getConnection().prepareStatement(querry, Statement.RETURN_GENERATED_KEYS);
		preparedStatement.setString(1, this.Name);
		preparedStatement.setString(2, this.Location);
		
		preparedStatement.setInt(3, this.Camid1);
		preparedStatement.setInt(4, this.Camid2);
		preparedStatement.setInt(5, this.Camid3);
		preparedStatement.setInt(6, this.Camid4);

		preparedStatement.setBoolean(7, this.Exist1);
		preparedStatement.setBoolean(8, this.Exist2);
		preparedStatement.setBoolean(9, this.Exist3);
		preparedStatement.setBoolean(10, this.Exist4);
		
		preparedStatement.executeUpdate();

		ResultSet tableKeys = preparedStatement.getGeneratedKeys();
		tableKeys.next();
		this.Id = tableKeys.getInt(1);
		preparedStatement.close();

		reloadEntity();
	}

	@Override
	public void updateEntity() throws SQLException {
		String querry = DataRepository.IntersectionUpdate;
		querry += " SET ";
		querry += "Name = ?, ";
		querry += "Location = ?, ";
		querry += "Camid1 = ?, ";
		querry += "Camid2 = ?, ";
		querry += "Camid3 = ?, ";
		querry += "Camid4 = ?, ";
		querry += "Exist1 = ?, ";
		querry += "Exist2 = ?, ";
		querry += "Exist3 = ?, ";
		querry += "Exist4 = ?";
		querry += " WHERE ";
		querry += "Id = ?";

		PreparedStatement preparedStatement = DataRepository.Connection.getConnection().prepareStatement(querry, Statement.RETURN_GENERATED_KEYS);
		preparedStatement.setString(1, this.Name);
		preparedStatement.setString(2, this.Location);
		preparedStatement.setInt(3, this.Camid1);
		preparedStatement.setInt(4, this.Camid2);
		preparedStatement.setInt(5, this.Camid3);
		preparedStatement.setInt(6, this.Camid4);
		preparedStatement.setBoolean(7, this.Exist1);
		preparedStatement.setBoolean(8, this.Exist2);
		preparedStatement.setBoolean(9, this.Exist3);
		preparedStatement.setBoolean(10, this.Exist4);

		preparedStatement.setInt(11, this.Id);

		preparedStatement.executeUpdate();
		preparedStatement.close();

		reloadEntity();
	}

	@Override
	public void deleteEntity() throws SQLException {
		String querry = DataRepository.IntersectionDelete;
		querry += " WHERE ";
		querry += "Id = ?";

		PreparedStatement preparedStatement = DataRepository.Connection.getConnection().prepareStatement(querry, Statement.RETURN_GENERATED_KEYS);
		preparedStatement.setInt(1, this.Id);

		preparedStatement.executeUpdate();
		preparedStatement.close();
	}

	@Override
	public int getId() {
		return this.Id;
	}

	@Override
	public void setId(int id) {
		this.Id = id;
	}
	
	public String getName() {
		return this.Name;
	}
	
	public void setName(String name) {
		this.Name = name;
	}
	
	public String getLocation() {
		return this.Location;
	}
	
	public void setLocation(String location) {
		this.Location = location;
	}
	
	public int getCamid1() {
		return this.Camid1;
	}
	
	public void setCamid1(int camid1) {
		this.Camid1 = camid1;
	}
	
	public int getCamid2() {
		return this.Camid2;
	}
	
	public void setCamid2(int camid2) {
		this.Camid2 = camid2;
	}
	
	public int getCamid3() {
		return this.Camid3;
	}
	
	public void setCamid3(int camid3) {
		this.Camid3 = camid3;
	}
	
	public int getCamid4() {
		return this.Camid4;
	}
	
	public void setCamid4(int camid4) {
		this.Camid4 = camid4;
	}
	
	public boolean getExist1() {
		return this.Exist1;
	}
	
	public void setExist1(boolean exist1) {
		this.Exist1 = exist1;
	}
	
	public boolean getExist2() {
		return this.Exist2;
	}
	
	public void setExist2(boolean exist2) {
		this.Exist2 = exist2;
	}
	
	public boolean getExist3() {
		return this.Exist3;
	}
	
	public void setExist3(boolean exist3) {
		this.Exist3 = exist3;
	}
	
	public boolean getExist4() {
		return this.Exist4;
	}
	
	public void setExist4(boolean exist4) {
		this.Exist4 = exist4;
	}
}
