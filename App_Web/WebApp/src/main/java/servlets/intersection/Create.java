package servlets.intersection;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import database.DataRepository;
import entity.Intersection;

public class Create extends HttpServlet {
	private static final long serialVersionUID = 1L;

	public Create() {
		super();
	}
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setAttribute("Error", request.getParameter("Error"));
		this.getServletContext().getRequestDispatcher("/WEB-INF/jsp/intersection/Create.jsp").forward( request, response );

	}
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try {
			String Name = request.getParameter("intersectionName");
			String Location = request.getParameter("intersectionLocation");
			
			System.out.println(request.getParameter("intersectionExist1"));
			
			int Camid1 = -1;
			int Camid2 = -1;
			int Camid3 = -1;
			int Camid4 = -1;
			
			boolean Exist1 = request.getParameter("intersectionExist1").equals("on");
			boolean Exist2 = request.getParameter("intersectionExist2").equals("on");
			boolean Exist3 = request.getParameter("intersectionExist3").equals("on");
			boolean Exist4 = request.getParameter("intersectionExist4").equals("on");
			
			if(Exist1) {
				Camid1 = Integer.parseInt(request.getParameter("intersectionCamid1"));
			}
			if(Exist2) {
				Camid2 = Integer.parseInt(request.getParameter("intersectionCamid2"));
			}
			if(Exist3) {
				Camid3 = Integer.parseInt(request.getParameter("intersectionCamid3"));
			}
			if(Exist4) {
				Camid4 = Integer.parseInt(request.getParameter("intersectionCamid4"));
			}

			Intersection intersection = new Intersection(Name, Location, Camid1, Camid2, Camid3, Camid4, Exist1, Exist2, Exist3, Exist4);
			intersection.createEntity();

			DataRepository.IntersectionDataSet.add(intersection);

			response.sendRedirect("Detail?Id="+intersection.Id);
		}
		catch (Exception e) {
			e.printStackTrace();
			response.sendRedirect("Create?Error=Impossible de creer l'entitee [intersection]");
		}
	}
}
