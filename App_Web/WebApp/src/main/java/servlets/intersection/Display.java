package servlets.intersection;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import database.DataRepository;
import entity.Intersection;

public class Display extends HttpServlet {
	private static final long serialVersionUID = 1L;

	public Display() {
		super();
	}
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try {
			int id = Integer.parseInt(request.getParameter("Id"));
			Intersection intersection = DataRepository.IntersectionDataSet.getById(id);
			if(intersection == null)
				throw new Exception();
			
			request.setAttribute("Error", request.getParameter("Error"));
			request.setAttribute("Intersection", intersection);
			this.getServletContext().getRequestDispatcher("/WEB-INF/jsp/intersection/Display.jsp").forward( request, response );
		}
		catch(Exception e) {
			response.sendRedirect("Index?Error=Impossible d'ouvrir la page");
			e.printStackTrace();
		}
	}
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}
}
