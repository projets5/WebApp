package servlets.intersection;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import database.DataRepository;
import entity.Intersection;

public class Edit extends HttpServlet {
	private static final long serialVersionUID = 1L;

    public Edit() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		try {
			int id = Integer.parseInt(request.getParameter("Id"));
			Intersection intersection = DataRepository.IntersectionDataSet.getById(id);
			if(intersection == null)
				throw new Exception();

			request.setAttribute("Error", request.getParameter("Error"));
			request.setAttribute("Intersection", intersection);
			this.getServletContext().getRequestDispatcher("/WEB-INF/jsp/intersection/Edit.jsp").forward( request, response );
		}
		catch(Exception e) {
			e.printStackTrace();
			response.sendRedirect("Index?Error=Impossible d'ouvrir la page");
		}
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		int id = 0;
		try
		{
			id = Integer.parseInt(request.getParameter("entityId"));
			if(id == 0)
				throw new Exception();

			try
			{
				Intersection intersection = DataRepository.IntersectionDataSet.getById(id);
				if(intersection == null)
					throw new Exception();

				String Name = request.getParameter("intersectionName");
				String Location = request.getParameter("intersectionLocation");
				
				int Camid1 = Integer.parseInt(request.getParameter("intersectionCamid1"));
				int Camid2 = Integer.parseInt(request.getParameter("intersectionCamid2"));
				int Camid3 = Integer.parseInt(request.getParameter("intersectionCamid3"));
				int Camid4 = Integer.parseInt(request.getParameter("intersectionCamid4"));
				
				boolean Exist1 = Boolean.parseBoolean(request.getParameter("intersectionExist1"));
				boolean Exist2 = Boolean.parseBoolean(request.getParameter("intersectionExist2"));
				boolean Exist3 = Boolean.parseBoolean(request.getParameter("intersectionExist3"));
				boolean Exist4 = Boolean.parseBoolean(request.getParameter("intersectionExist4"));
				
				intersection.Name = Name;
				intersection.Location = Location;
				
				intersection.Camid1 = Camid1;
				intersection.Camid2 = Camid2;
				intersection.Camid3 = Camid3;
				intersection.Camid4 = Camid4;
				
				intersection.Exist1 = Exist1;
				intersection.Exist2 = Exist2;
				intersection.Exist3 = Exist3;
				intersection.Exist4 = Exist4;

				intersection.updateEntity();

				response.sendRedirect("Detail?Id="+intersection.Id);
			}
			catch (Exception e)
			{
				response.sendRedirect("Edit?Id="+id+"&Error=Impossible de modifier l'entitee [intersection]");
			}
		}
		catch (Exception e)
		{
			response.sendRedirect("Index?Error=Id invalide");
		}
	}
}
