package servlets.intersection;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import database.DataRepository;
import entity.Intersection;

public class Delete extends HttpServlet {
	private static final long serialVersionUID = 1L;

	public Delete() {
		super();
	}
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try {
			int id = Integer.parseInt(request.getParameter("Id"));
			Intersection intersection = DataRepository.IntersectionDataSet.getById(id);
			if(intersection == null)
				throw new Exception();

			request.setAttribute("Error", request.getParameter("Error"));
			request.setAttribute("Intersection", intersection);
			this.getServletContext().getRequestDispatcher("/WEB-INF/jsp/intersection/Delete.jsp").forward( request, response );
		}
		catch(Exception e) {
			response.sendRedirect("Index?Error=Impossible d'ouvrir la page");
		}
	}
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		int id = 0;
		try
		{
			id = Integer.parseInt(request.getParameter("entityId"));
			if(id == 0)
				throw new Exception();

			try
			{
				Intersection intersection = DataRepository.IntersectionDataSet.getById(id);
				if(intersection == null)
					throw new Exception();

				intersection.deleteEntity();
				DataRepository.IntersectionDataSet.remove(intersection);
				
				response.sendRedirect("Index");
			}
			catch (Exception e)
			{
				response.sendRedirect("Delete?Id="+id+"&Error=Impossible de supprimer l'entitee [intersection]");
			}
		}
		catch (Exception e)
		{
			response.sendRedirect("Index?Error=Id invalide");
		}
	}
}