package listener;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import database.DataConnection;
import database.DataRepository;

public class MyAppServletContextListener implements ServletContextListener {

	
	public String _connectionString = "jdbc:h2:~/watatow;MODE=MySQL;";
	public String _initDatabase = "INIT=";
	
	@Override
	public void contextInitialized(ServletContextEvent arg0) {
		ServletContext context = arg0.getServletContext(); 
		_initDatabase += DataConnection.getInitialiserString(context.getResourceAsStream("/WEB-INF/create.sql"));
		
		System.out.println(_initDatabase);
		
		try {
			DataConnection databaseConnection = new DataConnection(_connectionString + _initDatabase, "root", "root");
			DataRepository.LoadDataRepository(databaseConnection);
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Override
	public void contextDestroyed(ServletContextEvent arg0) {

	}
}
